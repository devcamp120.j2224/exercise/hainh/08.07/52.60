import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j224java.Person;
import com.devcamp.j224java.Voucher2;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Voucher2> arraylist = new ArrayList<Voucher2>();

        Voucher2 voucher1 = new Voucher2();
        Voucher2 voucher2 = new Voucher2(3 , "Hải");
        Voucher2 voucher3 = new Voucher2(4 ,"Hoàng Hải" , 20000000 , new Date());
        Voucher2 voucher4 = new Voucher2(9 , "Nguyễn Hoàng Hải" , 500000000 , new Date() , false , new String[]{"tv","ipad","imac"} , new Person());

        arraylist.add(voucher1);
        arraylist.add(voucher2);
        arraylist.add(voucher3);
        arraylist.add(voucher4);
        
        voucher4.toString();
        voucher4.showData();
        
        for (Voucher2 voucher : arraylist){
            System.out.println(voucher.toString());
        }
    }
}

